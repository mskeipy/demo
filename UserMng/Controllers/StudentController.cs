﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserMng.Services;
using UserMng.ViewsModel.Student;

namespace UserMng.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;
        public StudentController(IStudentService  studentService)
        {
            _studentService = studentService;
        }
        
        /// <summary>
        /// show index student
        /// </summary>
        /// <returns>viewstudent</returns>   
        public async Task<IActionResult> Index()
        {
            var listStudent = await _studentService.GetList();
            return View(listStudent);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Post create student
        /// </summary>
        /// <param name="student"> StudentViewsModel </param>
        /// <returns>index else View</returns>
        [HttpPost]
        public async Task<IActionResult> Create(StudentViewsModel student)
        {
            if (ModelState.IsValid)
            {
                var addStudent = await _studentService.AddTEntity(student);
                return RedirectToAction("Index");
            }

            return View(student);
        }

        /// <summary>
        /// get id student
        /// </summary>
        /// <param name="id"> StudentViewsModel </param>
        /// <returns>view edit</returns>
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
                var getStudentId = await _studentService.GetIdTEntity(id);
            return View(getStudentId);
        }

        /// <summary>
        /// Post edit student
        /// </summary>
        /// <param name="student"> StudentViewsModel </param>
        /// <returns>index else View</returns>
        [HttpPost]
        public async Task<IActionResult> Edit(StudentViewsModel student)
        {
           
            var editStudent = await _studentService.EditTEntity(student);
            if (editStudent)
            {
                return RedirectToAction("Index");           
            }
            return BadRequest();
        }


        /// <summary>
        /// get delete
        /// </summary>
        /// <param name="id">student</param>
        /// <returns>index student</returns>
        [HttpGet]
        public async Task<IActionResult> Delete(int? Id)
        {
            if (Id==null)
            {
                return BadRequest();
            }
            var student = await _studentService.Delete(Id.Value);
            if (student)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        } 
    }
}