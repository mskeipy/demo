﻿using System.Collections.Generic;

namespace UserMng.ViewsModel.Class
{
    public class ClassViewsModel
    {
        public int ClassId { get; set; }
        public string Classname { get; set; }
        
        public List<Context.Student> Students { get; set; }
    }
}