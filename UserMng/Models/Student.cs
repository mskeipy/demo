﻿using System;
using UserMng.Models;

namespace UserMng.Context
{
    public class Student
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        
        public int ClassId { get; set; }
        public Class Class { get; set; }
    }
}