﻿using System.Collections.Generic;
using System.Threading.Tasks;


namespace UserMng.Services
{
    public interface IGenericReponsitory<TEntity>
    {
        
        Task<List<TEntity>> GetList();
        Task<bool> AddTEntity(TEntity addTEntity);
        IEnumerable<TEntity> GetTEntity();
        Task<TEntity> GetIdTEntity(int Id);
        Task<bool> EditTEntity(TEntity editTEntity);
        Task<bool> Delete(int Id);
        void Save();
    }
}