﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UserMng.Context;
using UserMng.ViewsModel.Student;

namespace UserMng.Services
{
    public interface IStudentService : IGenericReponsitory<StudentViewsModel>
    {
    }

    public class StudentService : GenericReponsitory<StudentViewsModel>, IStudentService
    {
//        private readonly UserMngDbContext _context;
//        private readonly IMapper _mapper;
//
//        public StudentService(UserMngDbContext context, IMapper mapper)
//        {
//            _context = context;
//            _mapper = mapper;
//        }
//        public IEnumerable<Student> GetStudent()
//        {
//            return _context.Students;
//        } 
//        
//        /// <summary>
//        /// list student service
//        /// </summary>
//        /// <returns>listStudent</returns>
//        public async Task<List<StudentViewsModel>> GetStudentListAsync()
//        {
//            var list = await _context.Students.ToListAsync();
//            var listStudent = _mapper.Map<List<StudentViewsModel>>(list);
//            return listStudent;
//        }
//        /// <summary>
//        /// add student service    
//        /// </summary>
//        /// <param name="addStudent">StudentViewsModel</param>
//        /// <returns>true || false</returns>
//        public async Task<bool> AddStudentAsync(StudentViewsModel addStudent)
//        {
//                var student = new Student()
//                {
//                    StudentName = addStudent.StudentName,
//                    ClassId = addStudent.ClassId
//                };
//                _context.Students.Add(student);
//                await _context.SaveChangesAsync();
//                return true; 
//        }
//
//        public void Save()
//        {
//            _context.SaveChangesAsync();
//        }
//        /// <summary>
//        /// get student service    
//        /// </summary>
//        /// <param name="id">StudentViewsModel</param>
//        /// <returns>findStudent</returns>
//        public async Task<StudentViewsModel> GetIdStudentAsync(int Id)
//        {
//            var findId = await _context.Students.FindAsync(Id);
//            var findStudent = _mapper.Map<StudentViewsModel>(findId);
//            return findStudent;
//
//        }
//      
//        public async Task<bool> EditStudentAsync(StudentViewsModel editsutden)
//        {
//            try
//            {
//                var student = await _context.Students.FindAsync(editsutden.StudentId);
//                student.StudentName = editsutden.StudentName;
//                student.ClassId= editsutden.ClassId;
//                _context.Update(student);
//                _context.SaveChanges();
//                return true;
//            }
//            catch (Exception e)
//            {
//                return false;
//            }         
//        }
//
//
//        public async Task<bool> Delete(int Id)
//        {
//            try
//            {
//                var student = await _context.Students.FindAsync(Id);
//                _context.Students.Remove(student);
//                _context.SaveChangesAsync();
//                return true;
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e);
//                throw;
//            }
//        }
        public StudentService(UserMngDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }

}
