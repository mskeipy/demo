﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UserMng.Context;


namespace UserMng.Services
{
    public class GenericReponsitory<TEntity> : IGenericReponsitory<TEntity> where TEntity : class 
    {
        private readonly UserMngDbContext _context;
        private readonly IMapper _mapper;

        public GenericReponsitory(UserMngDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        
        public  IEnumerable<TEntity> GetTEntity()
        {
            return _context.Set<TEntity>();
        }
        
        /// <summary>
        /// list TEntity service
        /// </summary>
        /// <returns>ListTEntity</returns>
        public async Task<List<TEntity>> GetList()
        {
            var list = await _context.Set<TEntity>().ToListAsync();
            var listTEntity = _mapper.Map<List<TEntity>>(list);
            return listTEntity;
        }
        /// <summary>
        /// add TEntity service    
        /// </summary>
        /// <param name="addTEntity">set<Tentity>()</param>
        /// <returns>true || false</returns>
        public async Task<bool> AddTEntity(TEntity addTEntity)
        {
            _context.Set<TEntity>().Add(addTEntity);
            await _context.SaveChangesAsync();
            return true; 
        }
  
        public void Save()
        {
            _context.SaveChangesAsync();
        }
        
        /// <summary>
        /// get TEntity service    
        /// </summary>
        /// <param name="id">TEntity</param>
        /// <returns>findTEntiry</returns>
        public async Task<TEntity> GetIdTEntity(int Id)
        {
            var findId = await _context.Set<TEntity>().FindAsync(Id);
            var findTEntity = _mapper.Map<TEntity>(findId);
            return findTEntity;

        }
        
        public async Task<bool> EditTEntity(TEntity editTEntity)
        {
            try
            {
                _context.Set<TEntity>().Update(editTEntity);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }         
        }
        
        public async Task<bool> Delete(int Id)
        {
            try
            {
                var entity = await _context.Set<TEntity>().FindAsync(Id);
                _context.Set<TEntity>().Remove(entity);
                _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}