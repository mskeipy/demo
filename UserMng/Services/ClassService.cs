﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UserMng.Context;
using UserMng.Models;
using UserMng.ViewsModel.Class;
using UserMng.ViewsModel.Student;

namespace UserMng.Services
{
    public interface IClassService
    {
        Task<List<ClassViewsModel>> GetClassListAsyn();
        IEnumerable<Class> GetClass();
        Task<bool> AddClassAsyn(ClassViewsModel addClass);
        Task<ClassViewsModel> GetIdClassAsyn(int Id);
        Task<bool> EditClassAsyn(ClassViewsModel editClass);
        Task<bool> DeleteClassAsyn(int Id);
        void Savechanges();
    }

    public class ClassService : IClassService
    {
        private readonly UserMngDbContext _context;
        private readonly IMapper _mapper;
        
        public ClassService(UserMngDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public IEnumerable<Class> GetClass()
        {
            return _context.Classes;
        }

        
        /// <summary>
        /// list class service
        /// </summary>
        /// <returns>listClass</returns>
        public async Task<List<ClassViewsModel>> GetClassListAsyn()
        {
            var list = await _context.Classes.ToListAsync();
            var listClass = _mapper.Map<List<ClassViewsModel>>(list);
            return listClass;
        }
        
        
        /// <summary>
        /// add class service    
        /// </summary>
        /// <param name="addClass">ClassViewsModel</param>
        /// <returns>true || false</returns>
        public async Task<bool> AddClassAsyn(ClassViewsModel addClass)
        {
            var Class = new Class()
            {
                Classname = addClass.Classname
            };
            _context.Classes.Add(Class);
            await _context.SaveChangesAsync();
            return true;
        }
        
        public void Savechanges()
        {
            _context.SaveChangesAsync();
        }
        
        /// <summary>
        /// get class service    
        /// </summary>
        /// <param name="id">ClassViewsModel</param>
        /// <returns>findClass</returns>
        public async Task<ClassViewsModel> GetIdClassAsyn(int Id)
        {
            var findId = await _context.Classes.FindAsync(Id);
            var findClass = _mapper.Map<ClassViewsModel>(findId);
            return findClass;
        }

        public async Task<bool> EditClassAsyn(ClassViewsModel editClass)
        {
            try
            {
                var Class = await _context.Classes.FindAsync(editClass.ClassId);
                Class.Classname = editClass.Classname;
                _context.Update(Class);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        
        public async Task<bool> DeleteClassAsyn(int Id)
        {
            try
            {
                var Class = await _context.Classes.FindAsync(Id);
                _context.Classes.Remove(Class);
                _context.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}